#!/usr/bin/env python
# coding: utf-8

# In[22]:


import matplotlib.pyplot as plt
from requests import options
import numpy as np
import csv
import scipy.integrate as integ
import sympy as sp
import sympy.plotting as splot
import pandas as pd

from scipy import misc
from scipy.spatial import ConvexHull
from scipy import optimize
from scipy.optimize import least_squares,minimize,Bounds,NonlinearConstraint, BFGS, LinearConstraint, SR1

from numpy import cos,sin,pi, arccos
from numpy.linalg import inv

from mpl_toolkits import mplot3d 
from mpl_toolkits.mplot3d import Axes3D

from sympy.sets import Interval

from sklearn import datasets

from sympy import solveset, symbols, Interval, Min

import seaborn as sns

from spatialmath import *
from roboticstoolbox import *

from Path import PathFrame

panda = models.DH.Panda()
panda.tool  = SE3.Tz(0.093)  # set center of a tool 93 cm in z axis !
q_neutral=(panda.qlim[0]+panda.qlim[1])/2
print(panda)
print("q_neutral = ",q_neutral)
# panda.plot(q_neutral,block=False)



ft=24 ; #Police size



# In[7]:
# ### Reference abscissa

nb_points_s = 6
s_pas=1/(nb_points_s-1)
s_list = np.arange(0,1+s_pas,s_pas)
print('s_list = ', s_list)
# s_list = np.array([0.4])


# In[8]:
# ### Rotation around wireloop

nb_points_alpha=20
alpha_min=-np.pi
alpha_max=(nb_points_alpha/2-1)*np.pi/(nb_points_alpha/2)
alpha_pas=(alpha_max-alpha_min)/(nb_points_alpha-1)
alpha_list = np.arange(alpha_min,alpha_max+alpha_pas,alpha_pas)
alpha_list

# In[27]:

def MGD(q):
    return panda.fkine(q)
def X(q):
    return panda.fkine(q).t[0]
def Y(q):
    return panda.fkine(q).t[1]
def Z(q_):
    return panda.fkine(q).t[2]
def Tcr(q):
    Twc = current_path_frame
    # print("s_i test = ", s_i)
    # print("Current Path frame = ", Twc)
    Tcr = Twc.inv() * MGD(q)
    # Tcr = SE3.Ry(Theta_y(q))œ
    return Tcr



# def Theta_y(q, Twc):
    
#     Tcr = Twc.inv() * MGD(q) 

#     r11 = Tcr.R[0,0]
#     r13 = Tcr.R[0,2]
        
#     theta_y = np.arctan2(r13, r11) 
#     print('theta_y(q) =', theta_y)
    
#     return theta_y

# def J0(q):
#     return panda.jacob0(q)

def OptFunction(x):
    # print("x=",x)
    # theta_y = f(q,s) = atan2(r13,r11) where, r11 and r13 come from Twc^-1 * Twr
    q = x
    r11 = Tcr(q).R[0,0]
    r21 = Tcr(q).R[1,0] 
    r31 = Tcr(q).R[2,0] 

    f = np.arctan2(-r31,np.sqrt(r11**2+r21**2))
    # print('fmin=',f)
    return f

def oppOptFunction(x):
    # print("x=",x)
    f = -OptFunction(x)
    # print('fmax=',f)
    return f

# In[]
# Constraints

lalala=[]

def Eq_values(q):
    tx = Tcr(q).t[0]
    ty = Tcr(q).t[1]
    tz = Tcr(q).t[2]
    r11 = Tcr(q).R[0,0]
    r21 = Tcr(q).R[1,0]
    r32 = Tcr(q).R[2,1]
    r33 = Tcr(q).R[2,2]
    q4_mean = (panda.qlim[1][3] + panda.qlim[0][3]) / 2
    theta_x = np.arctan2(r32,r33)
    theta_z = np.arctan2(r21,r11)
    test=[]
    test.append(tx)
    test.append(ty)
    eq_values = test#np.array([tx],[ty])#,tz])#,theta_x,theta_z, q4_mean]
    print(eq_values)
    lalala.append(eq_values)
    return eq_values

# def NL_constraints():
    
#     # do not contain constraint on value 1 for r22 !
#     ub = 1e8 * np.ones(4)
#     lb = -1e8 * np.ones(4)

#     nl_cons = NonlinearConstraint(Eq_values, lb, ub, jac='2-point', hess = BFGS())
#     return nl_cons

# def LIN_constraints():
    
#     # lb <= A X <= ub , X =[q1 q2 q3 q4 q5 q6 q7 theta_y]
#     ub = np.inf * np.ones(8)
#     lb = -np.inf * np.ones(8)
#     A = np.zeros((8,8))
#     # Add q_1 = 0 constraint
#     A[0,0] = 1
#     ub[0] = 0
#     lb[0] = 0
#     ###
#     lin_cons = LinearConstraint(A, lb, ub)
#     return lin_cons

def Eq_constraints_dict():
    eq_cons = {'type': 'eq',
                'fun': lambda x: Eq_values(x)}
    return eq_cons

def Ineq_constraints_dict():
    ineq_cons = {'type': 'ineq',
                'fun': lambda x: np.array([1])} # because always >= 0
    return ineq_cons




# In[37]:
theta_y_lim_df = pd.DataFrame(columns=['s','theta_y_min','theta_y_max','q_theta_y_min','q_theta_y_max','succ_min','succ_max'])


### BOUNDS
lb = panda.qlim[0]
ub = panda.qlim[1]
# FYI
# [-2.8973     -1.7628     -2.8973     -3.0718     -2.8973     -0.0175     -2.8973]
# [ 2.8973      1.7628      2.8973     -0.0698      2.8973      3.7525      2.8973]
print(lb)
print(ub)

global_bounds=[(lb[0]*20,ub[0]*20),(lb[1]*20,ub[1]*20),(lb[2]*20,ub[2]*20),(lb[3]*20,ub[3]*20),(lb[4]*20,ub[4]*20),(lb[5]*20,ub[5]*20),(lb[6]*20,ub[6]*20)]
bounds = Bounds(lb,ub)

print('s_list =',s_list)
s_test=0
ind = 0

# for s_i in s_list:
for i in range(2):
    #####################"
    # TODO REMOVE THAT !
    s_i=0.36
    ########################
    # Progression display
    print('si =',s_i)
    percentage=(ind/s_list.size*100)
    print(f'Computation along path: {percentage:.2f} %', end='\r')
    ind+=1

    ########################
    # Compute current Path Frame Twc
    current_path_frame=SE3(PathFrame(s_i),check=False) 
    
    ########################
    # Define initial guess
    # q_init=(panda.qlim[0]+panda.qlim[1])/2
    random7 = np.random.rand(7)
    q_init = panda.qlim[0] + random7 * (panda.qlim[1]-panda.qlim[0]) #randomly selected q vector inside its bounds
    
    ########################
    # # Minimization functions
    # q_theta_y_min = minimize(OptFunction, q_init, method='trust-constr', jac="2-point", hess=SR1(), bounds=bounds, constraints=[NL_constraints(), LIN_constraints()], options={'disp' : True, 'verbose':1})
    # q_theta_y_max = minimize(oppOptFunction, q_init, method='trust-constr', jac="2-point", hess=SR1(),bounds=bounds, constraints=[NL_constraints(), LIN_constraints()], options={'disp' : True, 'verbose':1})
    # q_theta_y_min = minimize(OptFunction, q_init, method='SLSQP',bounds=bounds,constraints=[Eq_constraints_dict(),Ineq_constraints_dict()], options={'disp' : True, 'verbose':1})
    # q_theta_y_max = minimize(oppOptFunction, q_init, method='SLSQP',bounds=bounds,constraints=[Eq_constraints_dict(),Ineq_constraints_dict()], options={'disp' : True, 'verbose':1})
    
    # # Global Minimization functions
    # SHGO
    # q_theta_y_min = optimize.shgo(OptFunction, global_bounds, constraints=Eq_constraints_dict(), options={'disp' : True}, minimizer_kwargs={'method':"SLSQP", 'constraints':[NL_constraints()]})
    # q_theta_y_max = optimize.shgo(oppOptFunction, global_bounds, constraints=Eq_constraints_dict(), options={'disp' : True}, minimizer_kwargs={'method':"SLSQP", 'constraints':[NL_constraints()]})
    q_theta_y_min = optimize.shgo(OptFunction, global_bounds, constraints=[Eq_constraints_dict(), Ineq_constraints_dict()], options={'disp' : True}, sampling_method='sobol')
    q_theta_y_max = optimize.shgo(oppOptFunction, global_bounds, constraints=[Eq_constraints_dict(),Ineq_constraints_dict()], options={'disp' : True}, sampling_method='sobol')
    plt.figure()
    plt.plot([x[0] for x in lalala],label='tx')
    plt.plot([x[1] for x in lalala],label='ty')    
    plt.show()

    # Differential Evolution
    # q_theta_y_min = optimize.differential_evolution(OptFunction, global_bounds,constraints=[NL_constraints()], disp=True)
    # q_theta_y_max = optimize.differential_evolution(oppOptFunction, global_bounds,constraints=[NL_constraints()], disp=True)
    
    print("Min SUCCESS =",q_theta_y_min)
    print("Max SUCCESS =",q_theta_y_max)

    print("Min SUCCESS =",q_theta_y_min.success)
    print("Max SUCCESS =",q_theta_y_max.success)

    print("Min q=",q_theta_y_min.x)
    print("Max q=",q_theta_y_max.x)
    
    ########################
    # Define theta_y max and min
        
    theta_y_min = q_theta_y_min.fun 
    theta_y_max = -q_theta_y_max.fun # theta_y_max is the opposite of the result from a minimization function, we want to maximize the value
    # Conditions on SUCCESS
    if (q_theta_y_min.success == False):
        theta_y_min = 0 * theta_y_min
    if (q_theta_y_max.success == False):
        theta_y_max = 0 * theta_y_max


    Twc = current_path_frame
    print("Tcr_min TEST=", Eq_values(q_theta_y_min.x) )
    print("Tcr_max TEST=",Eq_values(q_theta_y_max.x) )



    print("Tcr_min=",Twc.inv() * MGD(q_theta_y_min.x) )
    print("Tcr_min=",Tcr(q_theta_y_min.x) )
    print("Tcr_max=",Twc.inv() * MGD(q_theta_y_max.x))
    print("Tcr_max=",Tcr(q_theta_y_max.x))
    print("Tcr_min det=",(Twc.inv() * MGD(q_theta_y_min.x)).det() )
    print("Tcr_max det=",(Twc.inv() * MGD(q_theta_y_max.x)).det())

    # Transform radians into degrees
    theta_y_min = theta_y_min*180/np.pi
    theta_y_max = theta_y_max *180/np.pi

    print("Theta Min=",theta_y_min)
    print("Theta Max=",theta_y_max)

    ########################
    # Append in global dataframe
    theta_y_lim_df=theta_y_lim_df.append(pd.Series([s_i,theta_y_min,theta_y_max,q_theta_y_min.x,q_theta_y_min.x,q_theta_y_min.success,q_theta_y_max.success], index=theta_y_lim_df.columns), ignore_index=True)

    print('end opti')

print(theta_y_lim_df)


# #######################
# Export Data !
print("Exporting data")
theta_y_lim_df.to_csv("theta_y_lim_15_global_shgo_s036.csv")


# In[13]:



theta_y_lim_df=pd.read_csv('theta_y_lim_15_global_shgo_s036.csv',header=0)

print(theta_y_lim_df)

# fig = plt.figure(figsize=(10,10))
# plt.rc('xtick', labelsize=20) 
# plt.rc('ytick', labelsize=20) 

# fig.plot(theta_y_lim_df['s'],theta_y_lim_df['theta_y_min'],color='green')
# fig.plot(theta_y_lim_df['s'],theta_y_lim_df['theta_y_max'],color='red')
# plt.xlabel('s',fontsize=22)
# plt.ylabel('theta_y [°]',fontsize=22)
# plt.title('Theta_y angle bounds for path n°'+str(n_path),fontsize=22)

fig2 = plt.figure()
ax2 = fig2.add_subplot(1, 1, 1)
# ax2.plot(theta_y_lim_df['s'], theta_y_lim_df['theta_y_min'],color='green')
# ax2.plot(theta_y_lim_df['s'], theta_y_lim_df['theta_y_max'],color='red')
ax2.plot( theta_y_lim_df['theta_y_min'],color='green')
ax2.plot( theta_y_lim_df['theta_y_max'],color='red')
#ax2 = sns.lineplot(x="Time", y="RightHand_Y", hue="Trial", data=hand_pose_df)
#ax2 = sns.lineplot(x="Time", y="RightHand_Z", hue="Trial", data=hand_pose_df)
ax2.set_title(f'Theta_y angle bounds for path n°'+str(n_path) ,fontsize=22)
ax2.set_xlabel('s',fontsize=22)
ax2.set_ylabel('theta_y [°]',fontsize=22)

# plt.legend(fontsize='15', title_fontsize='40')
# plt.show()


# plt.legend(fontsize='15', title_fontsize='40')
plt.show()

print('Finished')