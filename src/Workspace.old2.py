#!/usr/bin/env python
# coding: utf-8

# In[22]:


import matplotlib.pyplot as plt
from requests import options
import numpy as np
import csv
import scipy.integrate as integ
import sympy as sp
import sympy.plotting as splot
import pandas as pd

from scipy import misc
from scipy.spatial import ConvexHull
from scipy import optimize
from scipy.optimize import least_squares,minimize,Bounds,NonlinearConstraint, BFGS, LinearConstraint

from numpy import cos,sin,pi, arccos
from numpy.linalg import inv

from mpl_toolkits import mplot3d 
from mpl_toolkits.mplot3d import Axes3D

from sympy.sets import Interval

from sklearn import datasets

from sympy import solveset, symbols, Interval, Min

import seaborn as sns

from spatialmath import *
from roboticstoolbox import *

panda = models.DH.Panda()
q_neutral=(panda.qlim[0]+panda.qlim[1])/2
print(panda)
print("q_neutral = ",q_neutral)
# panda.plot(q_neutral,block=False)



ft=24 ; #Police size


# In[23]:


n_path=15; # path number in "TrajectoryCharacteristics.ods" file
# VARIABLES
nb_points = 150 ;

#A*s+B*sp.sin(f1*s)+C*sp.cos(f1*s)+D*sp.sin(f2*s)**2+E*sp.cos(f2*s)**2+F*sp.sin(f3*s)**3+G*sp.cos(f3*s)**3 + H*s**2 + I*s**3
A=0;         #        #0
B=0;         # sin    #0
C=0;         # cos    #3/2
D=0;         # sin^2  #0
E=0;         # cos^2  #2
F=0;         # sin^3  #0
G=0;         # cos^3  #0
H=-2;         # x^2
I=0          # x^3

f1=3*sp.pi;    #6*sp.pi # Here is not frequency but pulsation, it should be written omega_1
f11=0;    #6*sp.pi
f12=0;    #6*sp.pi
f2=0;        #2*sp.pi
f3=0;        #0

dA=0;
dH=-0.5;
dI=0;

X_start=0.5;
Y_start=-0.2;
Z_start=0.2;

depth=1e-10; # trajectory depth in real space X
width=0.4; # trajectory width in real space Y
height=0.15 # trajectory height in real space Z


Z_t_max = 0 #sp.maximum(Z_t(s), s, S_Interval) # too complicated to compute

Z_t_min = -0.5 #sp.minimum(Z_t(s), s, S_Interval) # too complicated to compute


# Curvilinear abscissa
s = sp.Symbol('s') 
s_eval=np.arange(0,1,1/nb_points);
s_max = 1
s_min = 0
S_Interval=Interval(s_min,s_max)

# THEORETICAL FUNCTIONS
def X_t(s):
    return 1e-5*s
    #return 1e-5*s+A*s+B*sp.sin(f1*s)+C*sp.cos(f1*s)
    #return A*s+B*sp.sin(f1*s)+C*sp.cos(f1*s)
def Y_t(s):
    return s
def Z_t(s):
    #return 2*s
    return A*(dA+s)+B*sp.sin(f1*s)+C*sp.cos(f1*s)+D*sp.sin(f2*s)**2+E*sp.cos(f2*s)**2+F*sp.sin(f3*s)**3+G*sp.cos(f3*s)**3 + H*(dH+s)**2 + I*(dI+s)**3;
    #return A*s+B*sp.atan(sp.atan(sp.cos(f11*s)))+C*sp.atan(sp.atan(sp.cos(f12*s)))+D*sp.atan(sp.atan(sp.sin(f2*s)))**2+E*sp.atan(sp.atan(sp.cos(f2*s)))**2+F*sp.atan(sp.atan(sp.sin(f3*s)))**3+G*sp.atan(sp.atan(sp.cos(f3*s)))**3;

# COMPUTING
# splot.plot_parametric((Y_t(s),Z_t(s)),(s,s_min,s_max),xlabel="y_t(s)",ylabel='z_t(s)', title='Theoretical path - Z(s) =f(Y(s))')
# splot.plot_parametric((Y_t(s),X_t(s)),(s,s_min,s_max),xlabel="y_t(s)",ylabel='x_t(s)', title='Theoretical path - X(s) =f(Y(s))')
# splot.plot3d_parametric_line(X_t(s),Y_t(s),Z_t(s),(s,s_min,s_max),xlabel="x_t(s)",ylabel='y_t(s)',zlabel='z_t(s)', title='3D theoretical path')
print("x_t(s) = ",X_t(s))
print("y_t(s) = ",Y_t(s))
print("z_t(s) = ",Z_t(s))

# Computing Max Min of theoretical functions
X_t_max = sp.maximum(X_t(s), s, S_Interval)
print('X_t_max =\t',X_t_max)
X_t_min = sp.minimum(X_t(s), s, S_Interval)
print('X_t_min =\t',X_t_min)
Y_t_max = sp.maximum(Y_t(s), s, S_Interval)
print('Y_t_max =\t',Y_t_max)
Y_t_min = sp.minimum(Y_t(s), s, S_Interval)
print('Y_t_min =\t',Y_t_min)
print('Z_t_max =\t',Z_t_max)
print('Z_t_min =\t',Z_t_min)

# DERIVATIVE
dx_t=sp.diff(X_t(s),s)
dy_t=sp.diff(Y_t(s),s)
dz_t=sp.diff(Z_t(s),s)

# splot.plot(dz_t,(s,0,1),xlabel="s",ylabel='dz_t(s)', title='Derived theoretical path - dZ(s) =f(s)')
print("\ndx_t(s)/ds = ",dx_t)
print("dy_t(s)/ds = ",dy_t)
print("dz_t(s)/ds = ",dz_t)


# In[24]:


# REAL FUNCTIONS
def X_r(s):
    return (X_t(s)-X_t(0)) * depth / (X_t_max-X_t_min) + X_start +1e-10*s
def Y_r(s):
    return (Y_t(s)-Y_t(0)) * width / (Y_t_max-Y_t_min) + Y_start
def Z_r(s):
    return (Z_t(s)-Z_t(0)) * height / (Z_t_max-Z_t_min) + Z_start


# COMPUTING

# plot_size = max(depth,width,height)
# splot.plot(X_r(s),(s,s_min,s_max),xlabel="s",ylabel='x_r(s)', title='Real path - X =f(s)')
# splot.plot(Y_r(s),(s,s_min,s_max),xlabel="s",ylabel='y_r(s)', title='Real path - Y =f(s)')
# splot.plot(Z_r(s),(s,s_min,s_max),xlabel="s",ylabel='z_r(s)', title='Real path - Z =f(s)')
# splot.plot(Z_r(s),(s,0.5,0.7),xlabel="s",ylabel='z_r(s)', title='Real path - Z =f(s)')
# splot.plot_parametric((Y_r(s),Z_r(s)),(s,s_min,s_max),xlabel="y_r(s) [m]",ylabel='z_r(s) [m]', title='Real path - Z(s) =f(Y(s))')
# splot.plot3d_parametric_line(X_r(s),Y_r(s),Z_r(s),(s,s_min,s_max),xlim=[X_start+depth/2-plot_size/2,X_start+depth/2+plot_size/2],ylim=[Y_start+width/2-plot_size/2,Y_start+width/2+plot_size/2],zlim=[0.1,0.9],xlabel="x_r(s)",ylabel='y_r(s)',zlabel='z_r(s)', title='3D real path')
print("x_r(s) = ",X_r(s))
print("y_r(s) = ",Y_r(s))
print("z_r(s) = ",Z_r(s))

x_r=X_r(s)
y_r=Y_r(s)
z_r=Z_r(s)

# DERIVATIVE
dx_r=sp.diff(X_r(s),s)
dy_r=sp.diff(Y_r(s),s)
dz_r=sp.diff(Z_r(s),s)

# splot.plot(dz_r,(s,0,1),xlabel="s",ylabel='dz_r(s)', title='Derived real path - dZ =f(s)')
print("\ndx_r(s)/ds = ",dx_r)
print("dy_r(s)/ds = ",dy_r)
print("dz_r(s)/ds = ",dz_r)

# DOUBLE DERIVATIVE
ddx_r=sp.diff(dx_r,s)
ddy_r=sp.diff(dy_r,s)
ddz_r=sp.diff(dz_r,s)

# splot.plot(ddz_r,(s,0,1),xlabel="s",ylabel='ddz_r(s)', title='2nd Derived real path - ddZ =f(s)')
print("\nddx_r(s)/dds = ",ddx_r)
print("ddy_r(s)/dds = ",ddy_r)
print("ddz_r(s)/dds = ",ddz_r)

# Curvature

# Angle
alpha_x_r=sp.atan(dx_r)
alpha_y_r=sp.atan(dy_r)
alpha_z_r=sp.atan(dz_r)

# splot.plot(alpha_z_r,(s,0,1),xlabel="s",ylabel='alpha_z_r(s)', title='Orientation along the path - alpha =f(s)')
print("\nalpha_x_r(s) = ",alpha_x_r)
print("alpha_y_r(s) = ",alpha_y_r)
print("alpha_z_r(s) = ",alpha_z_r)


# Angle Derivative
dalpha_x_r=sp.diff(alpha_x_r,s)
dalpha_y_r=sp.diff(alpha_y_r,s)
dalpha_z_r=sp.diff(alpha_z_r,s)

# splot.plot(dalpha_z_r,(s,0,1),xlabel="s",ylabel='dalpha_z_r(s)', title='Derived orientation along the path - dalpha =f(s)')
print("\ndalpha_x_r(s) = ",dalpha_x_r)
print("dalpha_y_r(s) = ",dalpha_y_r)
print("dalpha_z_r(s) = ",dalpha_z_r)


# # Compute frame on each point of the curve

# In[25]:


def PathFrame(s_test):
    #print("s test = ",s_test)

    # TANGENT VECTOR
    r12 = -dx_r.evalf(subs={s:s_test})
    r22 = -dy_r.evalf(subs={s:s_test})
    r32 = -dz_r.evalf(subs={s:s_test})
    #r12 = -dx_r.evalf(abscissa_test)
    #r22 = -dy_r.evalf(abscissa_test)
    #r32 = -dz_r.evalf(abscissa_test)

    normT=sp.sqrt(r12**2+r22**2+r32**2)
    r12 = r12/normT
    r22 = r22/normT
    r32 = r32/normT

    #print("T = \n\t",r12,"\n\t",r22,"\n\t",r32)

    # NORMAL VECTOR
    r13 = 0 # on the plane Oy_0z_0, orthogonal to x_0 vector
    if(r32==0):
        r23=0
        r33=1
    else:
        r23 = sp.sqrt(1/((r22/r32)**2+1))
        r33 = -r23 * r22 / r32

    if (r33>0):
        r13=-r13
        r23=-r23
        r33=-r33

    #r23 = sp.sqrt(1/((r22/r12)**2+1))
    #r13 = -r23 * r22 / r12  # on the plane Oy_0x_0, orthogonal to x_0 vector
    #r33 = 0 # on the plane Oy_0x_0, orthogonal to x_0 vector
    #if (r13<0):
    #    r13=-r13
    #    r23=-r23
    #    r33=-r33

    #print("\nN = \n\t",r13,"\n\t",r23,"\n\t",r33)


    # 2nd NORMAL VECTOR
    r11 = r22*r33 - r32*r23
    r21 = r32*r13 - r12*r33
    r31 = r12*r23 - r22*r13
    #print("\nN2 = \n\t",r11,"\n\t",r21,"\n\t",r31)

    # Vectors norms
    #print("\nT norm = \t", sp.sqrt(r12**2+r22**2+r32**2))
    #print("N norm = \t", sp.sqrt(r13**2+r23**2+r33**2))
    #print("N2 norm = \t", sp.sqrt(r11**2+r21**2+r31**2))

    R=[[r11, r12, r13],[r21, r22, r23],[r31, r32, r33]]
    #print("\nR = \n", R[0],"\n", R[1],"\n", R[2])

    detR=R[0][1]*R[1][2]*R[2][0]-R[0][1]*R[2][2]*R[1][0]+R[1][1]*R[2][2]*R[0][0]-R[1][1]*R[0][2]*R[2][0]+R[2][1]*R[0][2]*R[1][0]-R[2][1]*R[1][2]*R[0][0]
    #print("\ndet(R) = ", detR)

    p=[X_r(s_test),Y_r(s_test),Z_r(s_test)]
    #print("\np = \n",p)
    return np.array([R[0]+[p[0]],R[1]+[p[1]],R[2]+[p[2]],[0.0,0.0,0.0,1.0]]).astype(float)


# In[7]:
# ### Reference abscissa

nb_points_s = 21
s_pas=1/(nb_points_s-1)
s_list = np.arange(0,1+s_pas,s_pas)
print('s_list = ', s_list)
# s_list = np.array([0.4])


# In[8]:
# ### Rotation around wireloop

nb_points_alpha=20
alpha_min=-np.pi
alpha_max=(nb_points_alpha/2-1)*np.pi/(nb_points_alpha/2)
alpha_pas=(alpha_max-alpha_min)/(nb_points_alpha-1)
alpha_list = np.arange(alpha_min,alpha_max+alpha_pas,alpha_pas)
alpha_list

# In[27]:

def MGD(q):
    return panda.fkine(q)
def X(q):
    return panda.fkine(q).t[0]
def Y(q):
    return panda.fkine(q).t[1]
def Z(q_):
    return panda.fkine(q).t[2]
def Tcr(q):
    Twc = current_path_frame
    Tcr = Twc.inv() * MGD(q)
    # Tcr = SE3.Ry(Theta_y(q))œ
    return Tcr

def Eq(x):
    q = x[0:7]
    Tcr = SE3.Ry(x[7])
    Twc = current_path_frame
    Eq =  Twc.inv() * MGD(q) * Tcr.inv()
    #print(Eq)
    return Eq

def Theta_y(q, Twc):
    
    Tcr = Twc.inv() * MGD(q) 

    r11 = Tcr.R[0,0]
    r13 = Tcr.R[0,2]
        
    theta_y = np.arctan2(r13, r11) 
    print('theta_y(q) =', theta_y)
    
    return theta_y

# def J0(q):
#     return panda.jacob0(q)

def OptFunction(x):
    # print("x=",x)
    f = x[7]
    # print('fmin=',f)
    return f

def oppOptFunction(x):
    # print("x=",x)
    f = - x[7]
    # print('fmax=',f)
    return f


# In[35]:
# ### Constraints

### NON LINEAR CONSTRAINTS

def Eq_values(x):
    t0 = Eq(x).t[0]
    t1 = Eq(x).t[1]
    t2 = Eq(x).t[2]
    r11 = Eq(x).R[0,0]
    r22 = Eq(x).R[1,1]
    r33 = Eq(x).R[2,2]
    eq_values = [t0,t1,t2,r11-1,r22-1,r33-1]
    return eq_values

def NL_constraints():
    
    # do not contain constraint on value 1 for r22 !
    ub = np.zeros(6)
    lb = np.zeros(6)

    nl_cons = NonlinearConstraint(Eq_values, lb, ub, jac='2-point', hess = BFGS())
    return nl_cons

def LIN_constraints():
    
    # lb <= A X <= ub , X =[q1 q2 q3 q4 q5 q6 q7 theta_y]
    ub = np.inf * np.ones(8)
    lb = -np.inf * np.ones(8)
    A = np.zeros((8,8))
    # Add q_1 = 0 constraint
    A[0,0] = 1
    ub[0] = 0
    lb[0] = 0
    ###
    lin_cons = LinearConstraint(A, lb, ub)
    return lin_cons

def Eq_constraints_dict():
    nl_cons = {'type': 'eq',
                'fun': lambda x: Eq_values(x)}
    return nl_cons

def Ineq_constraints_dict():
    nl_cons = {'type': 'ineq',
                'fun': lambda x: np.array([x[7]-theta_y_min])} # theta_y_max >= theta_y_min
    return nl_cons



# In[37]:
theta_y_lim_df = pd.DataFrame(columns=['s','theta_y_min','theta_y_max'])


### BOUNDS
lb = np.concatenate((panda.qlim[0],np.array([-np.pi])))
ub = np.concatenate((panda.qlim[1],np.array([np.pi])))
# FYI
# [-2.8973     -1.7628     -2.8973     -3.0718     -2.8973     -0.0175     -2.8973     -3.14159265]
# [ 2.8973      1.7628      2.8973     -0.0698      2.8973      3.7525      2.8973      3.14159265]
print(lb)
print(ub)

global_bounds=[(lb[0],ub[0]),(lb[1],ub[1]),(lb[2],ub[2]),(lb[3],ub[3]),(lb[4],ub[4]),(lb[5],ub[5]),(lb[6],ub[6]),(lb[7],ub[7])]
bounds = Bounds(lb,ub)

print('s_list =',s_list)
s_test=0
ind = 0

for s_i in s_list:
# for i in range(20):
    #####################"
    # TODO REMOVE THAT !
    # s_i=0.55
    ########################
    # Progression display
    print('si =',s_i)
    percentage=(ind/s_list.size*100)
    print(f'Computation along path: {percentage:.2f} %', end='\r')
    ind+=1

    ########################
    # Compute current Path Frame Twc
    current_path_frame=SE3(PathFrame(s_i),check=False)
    
    ########################
    # Define initial guess
    # q_init=(panda.qlim[0]+panda.qlim[1])/2
    random7 = np.random.rand(7)
    q_init = panda.qlim[0] + random7 * (panda.qlim[1]-panda.qlim[0])
    q_init[0] = 0
    print("q_init =", q_init)
    theta_y_init = Theta_y(q_init, current_path_frame)
    x_init = np.concatenate((q_init,np.array([theta_y_init])))
    
    ########################
    # # Minimization functions
    # q_theta_y_min = minimize(OptFunction, x_init, method='trust-constr', jac=[0,0,0,0,0,0,0,1], hess=BFGS(), bounds=bounds,constraints=[LIN_constraints(),NL_constraints()], options={'disp' : True, 'verbose':1})
    # q_theta_y_max = minimize(oppOptFunction, x_init, method='trust-constr', jac=[0,0,0,0,0,0,0,1], hess=BFGS(),bounds=bounds,constraints=[LIN_constraints(),NL_constraints()], options={'disp' : True, 'verbose':1})
    # q_theta_y_min = minimize(OptFunction, x_init, method='SLSQP',bounds=bounds,constraints=Eq_constraints_dict(), options={'disp' : True, 'verbose':1})
    # q_theta_y_max = minimize(oppOptFunction, x_init, method='SLSQP',bounds=bounds,constraints=[Eq_constraints_dict()], options={'disp' : True, 'verbose':1})
    
    # # Global Minimization functions
    # SHGO
    # q_theta_y_min = optimize.shgo(OptFunction, global_bounds,constraints=Eq_constraints_dict(), options={'disp' : True}, minimizer_kwargs={'method':"SLSQP"})
    # q_theta_y_max = optimize.shgo(oppOptFunction, global_bounds,constraints=Eq_constraints_dict(), options={'disp' : True}, minimizer_kwargs={'method':"SLSQP"})
    # Differential Evolution
    q_theta_y_min = optimize.differential_evolution(OptFunction, global_bounds,constraints=[LIN_constraints(),NL_constraints()], disp=True)
    q_theta_y_max = optimize.differential_evolution(oppOptFunction, global_bounds,constraints=[LIN_constraints(),NL_constraints()], disp=True)
    
    print("Min q=",q_theta_y_min.x)
    print("Max q=",q_theta_y_max.x)
    print("Min=",Eq(q_theta_y_min.x))
    print("Max=",Eq(q_theta_y_max.x))
    

    ########################
    # Define theta_y max and min
    theta_y_min = q_theta_y_min.x[7]
    theta_y_max = q_theta_y_max.x[7]
    

    ########################
    # Append in global dataframe
    theta_y_lim_df=theta_y_lim_df.append(pd.Series([s_i,theta_y_min,theta_y_max], index=theta_y_lim_df.columns), ignore_index=True)

    print("\ntheta_y_min= \n",theta_y_min*180/np.pi)
    print("\ntheta_y_max= \n",theta_y_max*180/np.pi)
    print("\ntheta_y_min 2= \n",q_theta_y_min.fun)
    print("\ntheta_y_max 2= \n",-q_theta_y_max.fun)

    print('end opti')

print(theta_y_lim_df)


# #######################
# Export Data !
print("Exporting data")
theta_y_lim_df.to_csv("theta_y_lim_15_global_de.csv")



# In[13]:



theta_y_lim_df=pd.read_csv('theta_y_lim_15_global_de.csv',header=0)

print(theta_y_lim_df)

# fig = plt.figure(figsize=(10,10))
# plt.rc('xtick', labelsize=20) 
# plt.rc('ytick', labelsize=20) 

# fig.plot(theta_y_lim_df['s'],theta_y_lim_df['theta_y_min']*180 / np.pi,color='green')
# fig.plot(theta_y_lim_df['s'],theta_y_lim_df['theta_y_max']*180 / np.pi,color='red')
# plt.xlabel('s',fontsize=22)
# plt.ylabel('theta_y [°]',fontsize=22)
# plt.title('Theta_y angle bounds for path n°'+str(n_path),fontsize=22)

fig2 = plt.figure()
ax2 = fig2.add_subplot(1, 1, 1)
# ax2.plot(theta_y_lim_df['s'], theta_y_lim_df['theta_y_min']*180 / np.pi,color='green')
# ax2.plot(theta_y_lim_df['s'], theta_y_lim_df['theta_y_max']*180 / np.pi,color='red')
ax2.plot( theta_y_lim_df['theta_y_min']*180 / np.pi,color='green')
ax2.plot( theta_y_lim_df['theta_y_max']*180 / np.pi,color='red')
#ax2 = sns.lineplot(x="Time", y="RightHand_Y", hue="Trial", data=hand_pose_df)
#ax2 = sns.lineplot(x="Time", y="RightHand_Z", hue="Trial", data=hand_pose_df)
ax2.set_title(f'Theta_y angle bounds for path n°'+str(n_path) ,fontsize=22)
ax2.set_xlabel('s',fontsize=22)
ax2.set_ylabel('theta_y [rad]',fontsize=22)

# plt.legend(fontsize='15', title_fontsize='40')
# plt.show()


# plt.legend(fontsize='15', title_fontsize='40')
plt.show()

print('Terminé')